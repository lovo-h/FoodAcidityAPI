package interfaces

import (
	"fmt"
	"gitlab.com/lovo-h/FoodAcidityAPI/domain"
	"gitlab.com/lovo-h/FoodAcidityAPI/usecases"
	"strconv"
)

// RepoFood helps retrieve USDA Food/Nutrition data from storage (DB).
type RepoFood struct {
	handlerCDB    HandlerCDB
	handlerLogger usecases.HandlerLogger
}

// FetchOneFoodByNdbNo returns the nutrition-info of some food, given
// that food-item's NdbNo (NDB Number).
func (roFood *RepoFood) FetchOneFoodByNdbNo(ndbNo string) ([]domain.Nutrient, error) {
	query := "SELECT ndb_no, nutr_no, nutr_val, units, nutrdesc FROM nutrition WHERE ndb_no = $1"
	queryParams := []interface{}{ndbNo}

	nutrientList := []domain.Nutrient{}

	getValidString := func(data []byte) string {
		if data == nil {
			return ""
		}
		return string(data)
	}

	queryParseFn := func(rawResults [][]byte) {
		nutrientList = append(nutrientList, domain.Nutrient{
			NdbNo:    getValidString(rawResults[0]),
			NutrNo:   getValidString(rawResults[1]),
			NutrVal:  getValidString(rawResults[2]),
            Units:    getValidString(rawResults[3]),
            NutrDesc: getValidString(rawResults[4]),
		})
	}

	queryErr := roFood.handlerCDB.Query(query, queryParams, queryParseFn)

	if queryErr != nil { // failed query
		roFood.handlerLogger.Error("Error occured in query",
			usecases.Fields{"query": query, "params": queryParams, "error": queryErr})
		return nil, queryErr
	}

    roFood.handlerLogger.Info("Valid FetchOneFoodByNdbNo query",
        usecases.Fields{"query": query, "params": queryParams})

    return nutrientList, nil
}

// FetchManyLongDescBySnippet returns a list of long-description snippets
// given a list of keyword-snippets.
func (roFood *RepoFood) FetchManyLongDescBySnippet(snippets []string) ([]domain.Snippet, error) {
	query := "SELECT ndb_no, long_desc, fdgrp_cd, fdgrp_desc FROM food WHERE long_desc ILIKE $1 "
	var queryParams []interface{}

	for idx, snip := range snippets {
		queryParams = append(queryParams, fmt.Sprintf("%s%s%s", "%%", snip, "%%"))

		if idx > 0 {
			query += "AND long_desc ILIKE $" + strconv.Itoa(idx+1) + " "
		}
	}

	query += "LIMIT 10;"

	snippetList := []domain.Snippet{}

	getValidString := func(data []byte) string {
		if data == nil {
			return ""
		}
		return string(data)
	}

	queryParseFn := func(rawResults [][]byte) {
		snippetList = append(snippetList, domain.Snippet{
			NdbNo:     getValidString(rawResults[0]),
			LongDesc:  getValidString(rawResults[1]),
			FdGrpCd:   getValidString(rawResults[2]),
			FdGrpDesc: getValidString(rawResults[3]),
		})
	}

	queryErr := roFood.handlerCDB.Query(query, queryParams, queryParseFn)

	if queryErr != nil { // failed query
		roFood.handlerLogger.Error("Error occured in query",
			usecases.Fields{"query": query, "params": queryParams, "error": queryErr})
		return nil, queryErr
	}

    roFood.handlerLogger.Info("Valid FetchManyLongDescBySnippet query",
        usecases.Fields{"query": query, "params": queryParams})

	return snippetList, nil
}

// RepoFoodBuilt returns a built RepoFood.
func RepoFoodBuilt(hrCDB HandlerCDB, hrLogger usecases.HandlerLogger) *RepoFood {
	roFood := new(RepoFood)
	roFood.handlerCDB = hrCDB
	roFood.handlerLogger = hrLogger

	return roFood
}
