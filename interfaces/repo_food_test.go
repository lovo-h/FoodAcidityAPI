package interfaces

import (
    "gitlab.com/lovo-h/FoodAcidityAPI/usecases"
    "reflect"
	"testing"
)

type MockHandlerCDB struct {
	sqlStr      string
	queryParams []interface{}
}

func (mhrCDB *MockHandlerCDB) Query(query string, queryParams []interface{}, fnSignal func([][]byte)) error {
	mhrCDB.sqlStr = query
	mhrCDB.queryParams = queryParams

	fnSignal([][]byte{[]byte("70705"), []byte("raw almonds"), []byte("1200"), []byte("Nut and Seed Products")})
	fnSignal([][]byte{[]byte("70706"), []byte("baked almonds"), []byte("1200"), []byte("Nut and Seed Products")})

	return nil
}

type MockHandlerLogger struct {}

func (mhrCDB *MockHandlerLogger) Info(msg string, fields usecases.Fields) {}
func (mhrCDB *MockHandlerLogger) Warn(msg string, fields usecases.Fields) {}
func (mhrCDB *MockHandlerLogger) Error(msg string, fields usecases.Fields) {}

// =-=-=-=-=-=-=-=-=-= INIT =-=-=-=-=-=-=-=-=-=

// =-=-=-=-=-=-=-=-=-= TESTS =-=-=-=-=-=-=-=-=-=

func TestRepoFoodManyLongDescBySnippet(t *testing.T) {
	repoFood := new(RepoFood)
	mhrCDB := new(MockHandlerCDB)
	mhrLogger := new(MockHandlerLogger)

	repoFood.handlerCDB = mhrCDB
	repoFood.handlerLogger = mhrLogger

	longDesc, getErr := repoFood.FetchManyLongDescBySnippet([]string{"raw"})

	if getErr != nil {
		t.Error(getErr)
		return
	}

	expectedQueryStr := "SELECT ndb_no, long_desc, fdgrp_cd, fdgrp_desc FROM food WHERE long_desc ILIKE $1 LIMIT 10;"

	if mhrCDB.sqlStr != expectedQueryStr || len(mhrCDB.queryParams) != 1 {
		t.Error("Incorrect query string || len(queryParams)")
		return
	}

	t0 := reflect.TypeOf(longDesc).String() != "[]domain.Snippet"
	t1 := len(longDesc) != 2
	t2 := longDesc[0].NdbNo != "70705"
	t3 := longDesc[1].NdbNo != "70706"

	if t0 || t1 || t2 || t3 {
		t.Error("invalid results")
	}
}
