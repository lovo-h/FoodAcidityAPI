package interfaces

// HandlerCDB interface defines the required functions that
// the HandlerCDB struct should have.
type HandlerCDB interface {
	Query(string, []interface{}, func([][]byte)) error
}

// HandlerSendGrid interface defines the required functions that
// the HandlerSendGrid struct should have.
type HandlerSendGrid interface {
	AddRecipient(recipient string) error
	EmailUser(userEmail, subject, plainEmailMessage, htmlEmailMessage string) error
}
