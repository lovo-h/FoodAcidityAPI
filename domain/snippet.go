package domain

// Snippet struct neatly packages food-description.
type Snippet struct {
    FdGrpCd   string `json:"fdGrpCd"`
    FdGrpDesc string `json:"fdGrpDesc"`
    LongDesc  string `json:"longDesc"`
    NdbNo     string `json:"ndbNo"`
}
