package domain

// Nutrient struct neatly packages nutrition-info.
type Nutrient struct {
    NdbNo    string `json:"ndbNo"`
    NutrNo   string `json:"nutrNo"`
    NutrVal  string `json:"nutrVal"`
    NutrDesc string `json:"nutrDesc"`
    Units    string `json:"units"`
}
