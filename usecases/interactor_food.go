package usecases

import (
	"fmt"
	"gitlab.com/lovo-h/FoodAcidityAPI/domain"
)

// InteractorFood defines all the uses-cases relating to food, of this service.
type InteractorFood struct {
	repoFood      RepoFood
	handlerLogger HandlerLogger
}

// OneFoodByNdbNo is a use-case of this service.
// Use-Case: returns nutrition-info of some given food, given an ndb-number.
func (irFood *InteractorFood) OneFoodByNdbNo(ndbNo string) ([]domain.Nutrient, error) {
	nutrientList, fetchErr := irFood.repoFood.FetchOneFoodByNdbNo(ndbNo)

	if fetchErr != nil { // failed to fetch food
		return nil, fmt.Errorf("Failed to fetch food from DB")
	}

	irFood.handlerLogger.Info("Succesfully fetched food from DB",
		Fields{"ndbno": ndbNo})

	return nutrientList, nil
}

// ManyLongDescBySnippet is a use-case of this service.
// Use-Case: returns a list of long-description snippets, given keyword-snippets.
func (irFood *InteractorFood) ManyLongDescBySnippet(descSnippet []string) ([]domain.Snippet, error) {
	snippetList, getErr := irFood.repoFood.FetchManyLongDescBySnippet(descSnippet)

	if getErr != nil { // failed to get long-descriptions
		return nil, fmt.Errorf("Failed to fetch long-descriptions from DB")
	}

	irFood.handlerLogger.Info("Succesfully fetched long-descriptions from DB",
		Fields{"descSnippet": descSnippet})

	return snippetList, nil
}

// InteractorFoodBuilt returns a built InteractorFood.
func InteractorFoodBuilt(roFood RepoFood, hrLogger HandlerLogger) *InteractorFood {
	irFood := new(InteractorFood)
	irFood.repoFood = roFood
	irFood.handlerLogger = hrLogger

	return irFood
}
