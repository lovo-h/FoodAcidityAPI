package usecases

import "gitlab.com/lovo-h/FoodAcidityAPI/domain"

// M holds key/val data in an easier to manage package.
type M map[string]interface{}

// RepoFood interface defines the required functions that
// the RepoFood struct should have.
type RepoFood interface {
	FetchOneFoodByNdbNo(string) ([]domain.Nutrient, error)
	FetchManyLongDescBySnippet([]string) ([]domain.Snippet, error)
}

// Fields holds key/val logging data
// in an easier to manage package.
type Fields map[string]interface{}

// HandlerLogger interface defines the required functions that
// the HandlerLogger struct should have.
type HandlerLogger interface {
	Info(msg string, fields Fields)
	Warn(msg string, fields Fields)
	Error(msg string, fields Fields)
}
