package usecases

import (
	"gitlab.com/lovo-h/FoodAcidityAPI/domain"
	"strings"
	"testing"
)

type MockLogger struct{}

func (mLogger *MockLogger) Info(msg string, fields Fields)  {}
func (mLogger *MockLogger) Warn(msg string, fields Fields)  {}
func (mLogger *MockLogger) Error(msg string, fields Fields) {}

type MockRepoFood struct {
	longSnippets []domain.Snippet
	foods        map[string]map[string]map[string]string
}

func (mroFood *MockRepoFood) FetchOneFoodByNdbNo(ndbNo string) ([]domain.Nutrient, error) {
	retFood := []domain.Nutrient{}

	if food, ok := mroFood.foods[ndbNo]; ok {
		proteinData := food["203"]
		calciumData := food["301"]

		proteinNutrData := domain.Nutrient{}
		calciumNutrData := domain.Nutrient{}

		proteinNutrData.NdbNo = ndbNo
		proteinNutrData.NutrNo = "203"
		proteinNutrData.NutrVal = proteinData["nutr_val"]
		proteinNutrData.Units = proteinData["units"]
		proteinNutrData.NutrDesc = proteinData["nutr_desc"]

		calciumNutrData.NdbNo = ndbNo
		calciumNutrData.NutrNo = "301"
		calciumNutrData.NutrVal = calciumData["nutr_val"]
		calciumNutrData.Units = calciumData["units"]
		calciumNutrData.NutrDesc = calciumData["nutr_desc"]

		retFood = append(retFood, proteinNutrData, calciumNutrData)
	}

	return retFood, nil
}

func (mroFood *MockRepoFood) FetchManyLongDescBySnippet(keywordList []string) ([]domain.Snippet, error) {
	snippetList := []domain.Snippet{}

	for _, longSnippet := range mroFood.longSnippets {

		for _, keyword := range keywordList {
			if strings.Contains(strings.ToLower(longSnippet.LongDesc), strings.ToLower(keyword)) {
				snippetList = append(snippetList, longSnippet)
			}
		}

	}

	return snippetList, nil
}

// =-=-=-=-=-=-=-=-=-= INIT =-=-=-=-=-=-=-=-=-=

func addFood2Map(foodsMap map[string]map[string]map[string]string, ndbNo, nutrValA, nutrValB string) {
	foodsMap[ndbNo] = map[string]map[string]string{
		"203": {
			"nutr_val":  nutrValA,
			"units":     "g",
			"nutr_desc": "Protein",
		},
		"301": {
			"nutr_val":  nutrValB,
			"units":     "mg",
			"nutr_desc": "Calcium, Ca",
		},
	}
}

func GetFoods() map[string]map[string]map[string]string {
	foods := map[string]map[string]map[string]string{}

	addFood2Map(foods, "12061", "21.15", "269")
	addFood2Map(foods, "11529", "0.88", "10")
	addFood2Map(foods, "05006", "18.6", "11")
	addFood2Map(foods, "11739", "2.98", "48")

	return foods
}

func newLongDescMap(ndbNo, longDesc, fdGrpCd, fdGrpDesc string) domain.Snippet {
	return domain.Snippet{
		NdbNo:     ndbNo,
		LongDesc:  longDesc,
		FdGrpCd:   fdGrpCd,
		FdGrpDesc: fdGrpDesc,
	}
}

func GetLongDescs() []domain.Snippet {
	longDescs := []domain.Snippet{}

	longDescs = append(longDescs, newLongDescMap("12061", "Nuts, almonds", "1200", "Nut and Seed Products"))
	longDescs = append(longDescs, newLongDescMap("11529", "Tomatoes, red, ripe, raw, year round average", "1100", "Vegetables and Vegetable Products"))
	longDescs = append(longDescs, newLongDescMap("05006", " Chicken, broilers or fryers, meat and skin, raw", "500", "Poultry Products"))
	longDescs = append(longDescs, newLongDescMap("11739", "Broccoli, leaves, raw", "1100", "Vegetables and Vegetable Products"))

	return longDescs
}

func newMockRepoFood() *MockRepoFood {
	repo := new(MockRepoFood)
	repo.longSnippets = GetLongDescs()
	repo.foods = GetFoods()

	return repo
}

// =-=-=-=-=-=-=-=-=-= TESTS =-=-=-=-=-=-=-=-=-=

func TestInteractorFoodManyLongDescBySnippet(t *testing.T) {
	irFood := new(InteractorFood)
	irFood.repoFood = newMockRepoFood()
	irFood.handlerLogger = new(MockLogger)

	longDescs, getErr := irFood.ManyLongDescBySnippet([]string{"raw"})

	if getErr != nil || len(longDescs) != 3 {
		t.Error("getErr OR len != 3")
	}
}

func TestInteractorFoodOneFoodByNDBNo(t *testing.T) {
	irFood := new(InteractorFood)
	irFood.repoFood = newMockRepoFood()
	irFood.handlerLogger = new(MockLogger)

	food, getErr := irFood.OneFoodByNdbNo("12061")

	// Fail if there's an error OR no food is returned OR if it's the wrong food
	if getErr != nil || len(food) == 0 || food[0].NutrVal != "21.15" {
		t.Error("getErr OR nutr_val != '21.15'")
		return
	}
}
