package infrastructure

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lovo-h/FoodAcidityAPI/usecases"
	"io"
	"os"
	"time"
)

// HandlerLogger interacts with the Logrus library.
type HandlerLogger struct {
	log log.Logger
}

// Info logs messages at the info-level.
func (hrLog *HandlerLogger) Info(msg string, fields usecases.Fields) {
	hrLog.log.WithFields(log.Fields(fields)).Info(msg)
}

// Warn logs messages at the warn-level.
func (hrLog *HandlerLogger) Warn(msg string, fields usecases.Fields) {
	hrLog.log.WithFields(log.Fields(fields)).Warn(msg)
}

// Error logs messages at the error-level.
func (hrLog *HandlerLogger) Error(msg string, fields usecases.Fields) {
	hrLog.log.WithFields(log.Fields(fields)).Error(msg)
}

// HandlerLoggerBuilt returns a built HandlerLogger.
func HandlerLoggerBuilt() *HandlerLogger {
	hrLog := new(HandlerLogger)

	fileName := fmt.Sprintf("%s_%d%d.log",
		"foodacidityapi", int(time.Now().Month()), time.Now().Year())
	file, openErr := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY, 0666)

	if openErr != nil {
		panic("Failed to create log-file")
	}

	hrLog.log = log.Logger{
		Out:       io.MultiWriter(os.Stdout, file),
		Hooks:     make(log.LevelHooks),
		Formatter: new(log.JSONFormatter),
		Level:     log.WarnLevel,
	}

	return hrLog
}
