package infrastructure

import (
	"encoding/json"
	"errors"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// HandlerSendGrid interacts with SendGrid through it's API.
type HandlerSendGrid struct {
	apiKey string
}

// AddRecipient adds a recipient to SendGrid, using the SendGrid API.
func (hrSendGrid *HandlerSendGrid) AddRecipient(recipientStr string) error {
	host := "https://api.sendgrid.com"
	endpoint := "/v3/contactdb/recipients"
	request := sendgrid.GetRequest(hrSendGrid.apiKey, endpoint, host)
	request.Method = "POST"
	request.Body = []byte(recipientStr)

	response, responseErr := sendgrid.API(request)

	if responseErr != nil {
		return errors.New("Error: failed to send message that would add recipient to SendGrid")
	}

	var respData map[string]interface{}
	jsonErr := json.Unmarshal([]byte(response.Body), &respData)

	if jsonErr != nil {
		return errors.New("AddRecipient: failed to unmarshal string to JSON")
	}

	var errorCount float64

	if respData["error_count"] != nil {
        errorCount = respData["error_count"].(float64)
    }

	if errorCount != 0 {
		return errors.New("Error: failed to create recipient on SendGrid")
	}

	return nil
}

// EmailUser emails a user a htmlEmailMessage, using SendGrid.
func (hrSendGrid *HandlerSendGrid) EmailUser(userEmail, subject, plainEmailMessage, htmlEmailMessage string) error {
	from := mail.NewEmail("FoodAcidity API", "foodacidityapi@email.com")
	to := mail.NewEmail("Guest User", userEmail)
	message := mail.NewSingleEmail(from, subject, to, plainEmailMessage, htmlEmailMessage)
	client := sendgrid.NewSendClient(hrSendGrid.apiKey)
	_, sendErr := client.Send(message)

	if sendErr != nil {
		return errors.New("Failed to send email to " + userEmail)
	}

	return nil
}

// HandlerSendGridBuilt returns a built HandlerSendGrid.
func HandlerSendGridBuilt(apiKey string) *HandlerSendGrid {
	handlerSendgrid := new(HandlerSendGrid)
	handlerSendgrid.apiKey = apiKey

	return handlerSendgrid
}
