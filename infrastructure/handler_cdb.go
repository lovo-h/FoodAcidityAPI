package infrastructure

import (
	"database/sql"
	// Needed to call pq's init function: register postgres.
	_ "github.com/lib/pq"
	"log"
)

// HandlerCockroachDB interacts with CockroachDB.
type HandlerCockroachDB struct {
	DB *sql.DB
}

// Query queries the DB, using the queryStr & queryParms. Then, returns the
// results through fnSignal, as raw-data.
func (hrCDB *HandlerCockroachDB) Query(queryStr string, queryParams []interface{}, fnSignal func([][]byte)) error {
	// Execute query
	rows, queryErr := hrCDB.DB.Query(queryStr, queryParams...)

	if queryErr != nil {
		log.Println(queryErr.Error())
		return queryErr
	}

	defer rows.Close()

	// Count columns & create DSes to store results
	cols, colsErr := rows.Columns()

	if colsErr != nil {
		return colsErr
	}

	rawResults := make([][]byte, len(cols))
	dest := make([]interface{}, len(cols))

	for idx := 0; idx < len(cols); idx++ {
		dest[idx] = &rawResults[idx]
	}

	// Read data
	for rows.Next() {
		if scanErr := rows.Scan(dest...); scanErr != nil {
			return scanErr
		}

		// Send string data
		fnSignal(rawResults)
	}

	return nil
}

// HandlerCockroachDBBuilt returns a built HandlerCockroachDB.
func HandlerCockroachDBBuilt() *HandlerCockroachDB {
	connStr := "postgresql://root@cockroach:26257/usdafooddb?sslmode=disable"
	db, dbErr := sql.Open("postgres", connStr)

	if dbErr != nil {
		log.Fatal(dbErr)
	}

	handlerCockroach := new(HandlerCockroachDB)
	handlerCockroach.DB = db

	return handlerCockroach
}
