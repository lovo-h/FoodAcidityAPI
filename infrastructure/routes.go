// TODO: implement SendGrid routes

package infrastructure

import (
    "github.com/99designs/gqlgen/handler"
    gmux "github.com/gorilla/mux"
    "gitlab.com/lovo-h/FoodAcidityAPI/infrastructure/graphql"
)

// RouterWithRoutesBuilt returns a gorilla-mux router with
// configured routes.
func RouterWithRoutesBuilt(resolver *graphql.Resolver) *gmux.Router {
	router := gmux.NewRouter().StrictSlash(false)
	apiRouter := router.PathPrefix("/api").Subrouter()

    apiRouter.Handle("/",
        handler.Playground("Food Acidity API Playground", "/api/query"))

    apiRouter.HandleFunc("/query",
        handler.GraphQL(graphql.NewExecutableSchema(graphql.Config{Resolvers: resolver})))

    //apiRouter.HandleFunc("/emails/edible_list", func(rw http.ResponseWriter, req *http.Request) {
	//	webservice.EmailUserEdibleList(rw, req)
	//}).Methods("POST")
    //
	//apiRouter.HandleFunc("/emails/recipients", func(rw http.ResponseWriter, req *http.Request) {
	//	webservice.AddRecipient(rw, req)
	//}).Methods("POST")

	return router
}
