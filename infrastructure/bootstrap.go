package infrastructure

import (
	"gitlab.com/lovo-h/FoodAcidityAPI/interfaces"
	"gitlab.com/lovo-h/FoodAcidityAPI/usecases"
    "gitlab.com/lovo-h/FoodAcidityAPI/infrastructure/graphql"
    "net/http"
	"os"
)

// FoodAcidityAPIInitialize starts up the FoodAcidity API.
func FoodAcidityAPIInitialize() {
	sendgridAPIKey := os.Getenv("SENDGRID_API_KEY")

	logger := HandlerLoggerBuilt()

	handlerCDB := HandlerCockroachDBBuilt()
	repoFood := interfaces.RepoFoodBuilt(handlerCDB, logger)
	interactorFood := usecases.InteractorFoodBuilt(repoFood, logger)

	sendgrid := HandlerSendGridBuilt(sendgridAPIKey)
    resolver := graphql.ResolverBuilt(*interactorFood, sendgrid, logger)
	router := RouterWithRoutesBuilt(resolver)

	server := &http.Server{
		Addr:    "0.0.0.0:3000",
		Handler: router,
	}

	if listenErr := server.ListenAndServe(); listenErr != nil {
		logger.Error("Server failed to boot",
			usecases.Fields{"error": listenErr.Error()})
	}
}
