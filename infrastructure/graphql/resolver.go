//go:generate gorunpkg github.com/99designs/gqlgen

package graphql

import (
    "context"
    "encoding/json"
    "fmt"
    "gitlab.com/lovo-h/FoodAcidityAPI/domain"
    "gitlab.com/lovo-h/FoodAcidityAPI/interfaces"
    "gitlab.com/lovo-h/FoodAcidityAPI/usecases"
    "html"
)

// Resolver is used by 'gqlgen' generated code to
// retrieve data using known methods.
type Resolver struct {
    InteractorFood  usecases.InteractorFood
    HandlerSendGrid interfaces.HandlerSendGrid
    HandlerLogger   usecases.HandlerLogger
}

// Mutation helps create MutationResolver.
func (r *Resolver) Mutation() MutationResolver {
    return &mutationResolver{r}
}

// Query helps create QueryResolver.
func (r *Resolver) Query() QueryResolver {
    return &queryResolver{r}
}

type mutationResolver struct{ *Resolver }

// AddRecipient receives pertinent recipient info from the frontend
// and sends it to SendGrid for storage.
func (r *mutationResolver) AddRecipient(ctx context.Context, firstName string, lastName string, email string) (bool, error) {
    recipientData, marshalErr := json.Marshal([]map[string]string{{
        "email":      email,
        "first_name": firstName,
        "last_name":  lastName,
    }})

    if marshalErr != nil { // failed to marshal JSON
        r.HandlerLogger.Error("Failed to marshal JSON", usecases.Fields{
            "recipientData": recipientData,
        })
        return false, marshalErr
    }

    addErr := r.HandlerSendGrid.AddRecipient(string(recipientData))

    if addErr != nil { // failed to add recipient
        r.HandlerLogger.Error("Error at AddRecipient using SendGrid", usecases.Fields{
            "recipientData": string(recipientData),
        })

        return false, addErr
    }

    r.HandlerLogger.Info("Successfully added recipient", usecases.Fields{
        "recipientData": string(recipientData),
    })

    return true, nil
}

// EmailUserEdibleList receives a list of edibleFoods from the frontend and
// emails it to an email provided from the frontend.
func (r *mutationResolver) EmailUserEdibleList(ctx context.Context, edibleFoods []string, email string) (bool, error) {
    var plainEmailMsg string
    var htmlEmailMsg string

    for foodIdx, foodName := range edibleFoods {
        safeFood := html.EscapeString(foodName)
        plainEmailMsg += fmt.Sprintf("%d %s\n", foodIdx+1, safeFood)
        htmlEmailMsg += fmt.Sprintf("<li>%s</li>", safeFood)
    }

    plainEmailMsg = fmt.Sprintf("Personalized Food List:\n%s", plainEmailMsg)
    htmlEmailMsg = fmt.Sprintf(
        "<span style='font-weight: bold'> Personalized Food List:</span><br/><ul>%s</ul>", htmlEmailMsg)
    subjectLine := "Personalized Food List To Decrease Mortality Rate"

    emailErr := r.HandlerSendGrid.EmailUser(email, subjectLine, plainEmailMsg, htmlEmailMsg)

    if emailErr != nil { // failed to email user
        r.HandlerLogger.Error("Error at EmailUser using SendGrid", usecases.Fields{
            "email": email,
            "subject": subjectLine,
            "body": plainEmailMsg,
        })

        return false, emailErr
    }

    r.HandlerLogger.Info("EmailUserEdibleList", usecases.Fields{"plainEmailMsg": plainEmailMsg})

    return true, nil
}

type queryResolver struct{ *Resolver }

// Nutrients helps return a list of Nutrient data.
func (r *queryResolver) Nutrients(ctx context.Context, ndbNo string) ([]domain.Nutrient, error) {
    return r.InteractorFood.OneFoodByNdbNo(ndbNo)
}

// LongDescs helps return a list of Snippet data.
func (r *queryResolver) LongDescs(ctx context.Context, keywords []string) ([]domain.Snippet, error) {
    return r.InteractorFood.ManyLongDescBySnippet(keywords)
}

// ResolverBuilt returns a built HandlerCockroachDB.
func ResolverBuilt(irFood usecases.InteractorFood, sendgrid interfaces.HandlerSendGrid, hrLogger usecases.HandlerLogger) *Resolver {
    resolver := &Resolver{}
    resolver.InteractorFood = irFood
    resolver.HandlerSendGrid = sendgrid
    resolver.HandlerLogger = hrLogger

    return resolver
}
