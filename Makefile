# TODO: Add code-coverage
# TODO: Export coverage report to HTML
# TODO: Finish the target: prepare

# The name of the executable (default is current directory name)
TARGET := foodacidityapi
.DEFAULT_GOAL := $(TARGET)

VERSION := 1.0.0
BUILD := $(shell git rev-parse HEAD)

# Use linker flags to provide version/build settings to the target
LDFLAGS=-ldflags "-X=main.Version=$(VERSION) -X=main.Build=$(BUILD)"

# go source files, ignore vendor directory
GOFILES = $(shell find . -name '*.go' -not -path "./vendor/*")

# go packages, ignore vendor directory & test files
GOPACKAGES = $(shell go list ./...  | grep -v /vendor/ | grep -v _test.go)

all: clean deps lint test $(TARGET) ## Prepare everything (for commit)

# prepare:
# clean test vet simplify lint

$(TARGET): $(GOFILES) ## Target binary: foodacidityapi
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a $(LDFLAGS) -o $(TARGET)

build: $(TARGET) ## Build the binary file
	@true

deps: ## Get the dependencies
	if ! hash golint 2>/dev/null; then go get -u golang.org/x/lint/golint; fi

lint: ## Lint the files
	@golint -set_exit_status $(GOPACKAGES)

test: ## Run unittests
	@go test -short $(GOPACKAGES)

race: ## Run data race detector
	@go test -race -short $(GOPACKAGES)

# -msan is not supported on darwin/amd64
msan: ## Run memory sanitizer
	@go test -msan -short $(GOPACKAGES)

clean:
	-rm -f $(TARGET)

help: ## Display the help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# Makefile targets are "file targets" - they are used to build files from other files.
# By adding .PHONY, it tells "make" that the following targets are not associate with files.
.PHONY: all build deps lint test race msan clean help
