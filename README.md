# FoodAcidityAPI
[![Build Status](https://gitlab.com/lovo-h/FoodAcidityAPI/badges/master/build.svg)](https://gitlab.com/lovo-h/FoodAcidityAPI/commits/master)
[![Coverage Report](https://gitlab.com/lovo-h/FoodAcidityAPI/badges/master/coverage.svg)](https://gitlab.com/lovo-h/FoodAcidityAPI/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/lovo-h/FoodAcidityAPI)](https://goreportcard.com/report/gitlab.com/lovo-h/FoodAcidityAPI)

This FoodAcidity API will be used by the [FoodAcidity frontend app](https://gitlab.com/lovo-h/FoodAcidity) 
to retrieve nutritional information for some given food and to interact with [SendGrid](https://sendgrid.com/). 
The nutritional info has been collected from the 
[USDA's Food Composition Database](https://ndb.nal.usda.gov/ndb/search/list).

## How to Use This Service

To be able to boot-up this FoodAcidity API, import the `infrastructure` package,
which contains the function `FoodAcidityAPIInitialize` that initializes the API.

```cgo
package main

import "gitlab.com/lovo-h/FoodAcidityAPI/infrastructure"

func main() {
  infrastructure.FoodAcidityAPIInitialize()
}
```

## How To Run This Service 

You can either run this library by building it with Go's build-tools
and running the compiled binary. Or, you can use Docker to run the 
`docker-compose.yml` file.

### Go Build-Tools

To run this library by building a binary, first compile the code with
the following command: `make`. Then run your binary with the 
following command: `./foodacidityapi`.

### Docker

To run the this library, using Docker, run the following command at
the root of this library's directory: `docker-compose up`.
